@extends('layouts.base')
@section('content')

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 main-div-border">
            <div class="row">
                <div class="col-sm-3 back-back">
                    <a href="{{ url('/promotions') }}" class="back-link"><i class="fa fa-chevron-left"
                                                                            aria-hidden="true"></i>&nbsp;Back</a>
                </div>
                <div class="col-sm-9 body-back">
                    <h3 class="all-news">Create Promotion</h3>
                    <div class="row">
                        <form action="{{ url('/savePromotions') }}" method="post" enctype="multipart/form-data">
                            <div class="col-sm-5">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name
                                        &nbsp;<span>(Characters Left: 250)</span></label>
                                    <input type="text" class="form-control" id="name" name="name">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Offer
                                        &nbsp;<span>(Characters Left: 50)</span></label>
                                    <input type="text" class="form-control" id="offer" name="offer">
                                    @if ($errors->has('offer'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('offer') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Description</label>
                                    <textarea class="form-control" rows="8" id="description"
                                              name="description"> </textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Upload Image</label>
                                    <div class="input-group">

                                        <input type="text" class="form-control" readonly>
                                        <label class="input-group-btn">
                    <span class="btn btn-primary">
                        Upload<input type="file" name="image" style="display: none;" multiple id="banner_image">
                    </span>
                                        </label>
                                    </div>
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                        </span>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 border">
                                        <img src="images/preview.jpg" id="image_preview" class="img-responsive"/>
                                    </div>
                                </div>
                            </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-4 text-left">
                            {{--<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal">DELETE
                                PROMOTION</a>--}}
                        </div>
                        <div class="col-xs-8 text-right">
                            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal2">CANCEL</a>
                            <button type="submit" class="btn btn-success">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </form>


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <h3>Confirm Delete</h3>
                    <p>Are you sure you want to permantly delete these files?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-danger">DELETE</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <h4 class="text-center green-text">Login Success</h4>
                    <h5 class="text-center"><i class="fa fa-check-circle-o fa-5x green-text text-center"
                                               aria-hidden="true"></i></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                </div>
            </div>
        </div>
    </div>

    <!--End  Modal -->


    <!-- Modal -->
    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <h4 class="text-center red-text">Login Failed</h4>
                    <h5 class="text-center"><i class="fa fa-times-circle-o fa-5x red-text text-center"
                                               aria-hidden="true"></i></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function () {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready(function () {
                $(':file').on('fileselect', function (event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    } else {
                        if (log) alert(log);
                    }

                });
            });

        });

        //image preview
        $("#banner_image").change(function () {
            readURL(this);
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection

