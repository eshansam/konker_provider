@extends('layouts.base')
@section('content')

<div class="row">
    <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Promotions</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
</div>
<div class="row gradiant-back">
    <div class="col-sm-10 col-sm-offset-1 gray-back main-div-border">
        <div class="row">
            <div class="col-sm-3">
                <div class="col-sm-12 hidden-xs">
                    <ul class="nav nav-pills nav-stacked">
                        <li role="presentation" class="active"><a href="{{ url('/promotions') }}">Promotions</a></li>
                    </ul>
                </div>

            </div>
            <div class="col-sm-9 body-back">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="all-news">All Promotions</h3>
                    </div>
                </div>
                <div class="row bot-pad-row">
                    <div class="col-sm-10 col-xs-6">
                        <a href="{{ url('/createPromotions') }}"><button class="btn btn-success" type="button">create a promotions</button></a>
                    </div>
                    <div class="col-sm-2 filter-pad  col-xs-6"></div>
                </div>
                <div class="row row-mar">
                    @foreach( $offers as $offer)
                        <div class="col-sm-4 box-gen-pad img-div">
                            <div class="row pad-box">
                                <div class="col-sm-12 box-back-wrap">
                                    <div class="glyphicon glyphicon-remove delete_coross cls-btn" data-offer-id="{{ $offer->id }}"></div>
                                    <img src="{{$offer->img_url}}" class="img-responsive box-back-img">

                                    <div class="col-sm-12 box-inner-pad">
                                        <h4 class="bx-title">&nbsp;</h4>
                                        <h4 class="bx-title">{{$offer->offer}}</h4>
                                        <p class="bx-para">{{$offer->description}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--<div class="col-sm-4 box-gen-pad">
                        <div class="row pad-box">
                            <div class="col-sm-12 empty-box-back">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 box-gen-pad">
                        <div class="row pad-box">
                            <div class="col-sm-12 empty-box-back">
                            </div>
                        </div>
                    </div>--}}
                </div>
                {{--<div class="row row-mar">
                    <div class="col-sm-4 box-gen-pad">
                        <div class="row pad-box">
                            <div class="col-sm-12 empty-box-back">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 box-gen-pad">
                        <div class="row pad-box">
                            <div class="col-sm-12 empty-box-back">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 box-gen-pad">
                        <div class="row pad-box">
                            <div class="col-sm-12 empty-box-back">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-mar">
                    <div class="col-sm-4 box-gen-pad">
                        <div class="row pad-box">
                            <div class="col-sm-12 empty-box-back">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 box-gen-pad">
                        <div class="row pad-box">
                            <div class="col-sm-12 empty-box-back">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 box-gen-pad">
                        <div class="row pad-box">
                            <div class="col-sm-12 empty-box-back">
                            </div>
                        </div>
                    </div>
                </div>--}}
            </div>
        </div>
    </div>
</div>


    <script>

        // Delete Image
        $('.cls-btn').click(function () {

            var delete_img = $(this);
            var offer_id = $(this).data('offer-id');
            // Confirmation
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Image!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d93a34",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            method: "GET",
                            url: "{{ url('/delete_offer') }}",
                            data: {offer_id: offer_id},
                            cache: false,
                            success: function (data) {
                                if ("SUCCESS") {
                                    delete_img.closest('.img-div').remove();
                                    swal("Deleted!", "Image has been deleted.", "success");
                                } else {
                                    swal('Error!', 'Image has not been deleted', 'error');
                                }
                            },
                            error: function (data) {
                                swal('Error!', 'Image has not been deleted', 'error');
                            }
                        });
                    }
                }
            );

        });


</script>

@endsection