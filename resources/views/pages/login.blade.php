@extends('layouts.plain')
@section('content')
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 pad-login">
        <div class="row">
            <div class="col-sm-12 login-shadow">
                <h3 class="login-text">Login</h3>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    <div class="form-group">
                        {{ csrf_field() }}
                        <label for="email" class="form-lable">Username</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="password" class="form-lable">Password</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 login-btn-pad">
                            <button type="submit" class="btn btn-danger col-sm-12 login-btn">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection