<div class="row head">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="row">
            <div class="col-sm-3 text-center">
                <img src="images/logo.png" height="40px" class="logo">
                <h3 class="logo-text">&nbsp;</h3>
            </div>

            <div class="col-sm-8 pull-right text-right">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle log-name" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="{{ url('/logout') }}">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
                {{--<p href="#" class="log-name">Ruwan Hettiarachchi</p>
                <a href="#" class="log">Logout</a>--}}
            </div>
        </div>
    </div>
</div>