<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>All in one</title>
<script src="{!! asset('js/jquery-1.12.4.js')!!}"></script>
<script src="{!! asset('js/bootstrap.min.js')!!}"></script>

<link href="{!! asset('css/bootstrap.min.css') !!}" rel="stylesheet">
<link href="{!! asset('css/font-awesome.min.css') !!}" rel="stylesheet">
<script src="{{ URL::asset('sweetalert2/sweetalert2.min.js')}}"></script>
<link href="{{ URL::asset('sweetalert2/sweetalert2.min.css')}}" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.8/typicons.min.css' rel='stylesheet' type='text/css'>
<link href="{!! asset('style.css')!!}" rel="stylesheet">







