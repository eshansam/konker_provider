<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm');
/*Route::get('/dashboard', 'HomeController@dashboard');*/
Route::get('/promotions', 'PromotionsController@promotions');
Route::get('/createPromotions', 'PromotionsController@createPromotion');
Route::post('/savePromotions', 'PromotionsController@savePromotion');
Route::get('/delete_offer', 'PromotionsController@deletePromotion');

Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');