<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login','Api\LoginController@authenticate');
Route::post('register','Api\ApiController@userRegister');
Route::post('update_user','Api\ApiController@updateUserdata');
Route::post('get_user_data','Api\ApiController@getUserdata');
Route::post('favorite_offers','Api\ApiController@addFavorites');

Route::get('get_offers','Api\ApiController@getOffers');
Route::post('get_leaderboard','Api\ApiController@getLeaderboard');