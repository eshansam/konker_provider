<?php

namespace App\Http\Controllers\Api;

use App\Favorites;
use App\Mail\FavoritesAdded;
use App\Offers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;


class ApiController extends Controller
{
    public function userRegister(Request $request)
    {

        $checkmail = User::where('email', $request->email)->first();


        if ($checkmail) {
            return response()->json(['result' => ['status' => 'false', 'massage' => 'email already exsists']], 401);
        } else {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);

            if ($user) {
                return response()->json(['result' => ['status' => 'true', 'massage' => 'registred successfully']], 201);
            } else {
                return response()->json(['result' => ['status' => 'false', 'massage' => 'error in db connection']], 401);
            }

        }


    }


    public function updateUserdata(Request $request)
    {

        $checkmail = User::where('email', $request->email)->first();


        $user = $request->user_data;
        $score = $request->score;

        if ($user) {

            if ($checkmail) {

                $userupdate = User::where('email', $request->email)->update(['user_data' => $request->user_data, 'score' => $score]);
                if ($userupdate) {
                    return response()->json(['result' => ['status' => 'true', 'massage' => 'user data updated successfully']], 201);
                }
                return response()->json(['result' => ['status' => 'false', 'massage' => 'error in db connection']], 401);

            } else {

                return response()->json(['result' => ['status' => 'false', 'massage' => 'email not found']], 401);
            }

        } else {

            return response()->json(['result' => ['status' => 'false', 'massage' => 'no user data']], 401);

        }

    }

    public function getUserdata(Request $request)
    {


        $checkmail = User::where('email', $request->email)->first();


        if ($checkmail) {

            $userdata = User::where('email', $request->email)->first();
            $udata = $userdata->user_data;
            $uscore = $userdata->score;

            $data = array();
            $data["user_data"] = $udata;

            $manage = json_decode($udata);

            if ($udata) {

                return response()->json(['result' => ['status' => 'true', 'user_data' => $manage, 'score' => $uscore]], 201);

            } else {

                return response()->json(['result' => ['status' => 'false', 'user_data' => []]], 401);


            }
        } else {

            return response()->json(['result' => ['status' => 'false', 'massage' => 'email not found']], 401);
        }


    }


    public function getOffers()
    {
        $Offers = array();
        $Offers = Offers::select('*')->getQuery()->orderBy('created_at', 'desc')->take(10)->get();

        if ($Offers) {

            return response()->json(['result' => ['status' => 'true', 'offers' => $Offers]], 201);
        } else {

            return response()->json(['result' => ['status' => 'false', 'offers' => []]], 401);
        }


    }

    public function getLeaderboard(Request $request)
    {

        $start = $request->start;
        $count = $request->count;

        if ($count) {


            $scores = User::select('id','name', 'score')->getQuery()->orderBy('score', 'DESC')->offset($start)->take($count)->get();

            if ($scores) {
                return response()->json(['result' => ['status' => 'true', 'scores' => $scores]], 201);
            } else {
                return response()->json(['result' => ['status' => 'false', 'scores' => []]], 401);
            }


        } else {

            return response()->json(['result' => ['status' => 'false', 'scores' => []]], 401);
        }

    }


    public function addFavorites(Request $request)
    {

        $checkmail = User::where('email', $request->email)->first();

        if ($checkmail) {

            $adminmail = "eshan.he@gmail.com";

            $offer = $request->offer_id;
            $email = $request->email;

            $checkfav = Favorites::where('offer',$offer)->where('email',$email)->get();

            if(!$checkfav->isEmpty()){

                return response()->json(['result' => ['status' => 'false', 'massage' => 'Already in favorite list']], 400);

            }else {

                $checkoffer = Offers::where('id',$offer)->get();

                if($checkoffer->isEmpty()){

                    return response()->json(['result' => ['status' => 'false', 'massage' => 'Offer Not exists']], 400);

                }else {

                    $favorite = Favorites::create([
                        'email' => $email,
                        'offer' => $offer,
                    ]);

                    if ($favorite) {

                        Mail::to($adminmail)->send(new FavoritesAdded($checkoffer[0], $checkmail));

                        return response()->json(['result' => ['status' => 'true', 'massage' => 'Added to favorites successfully']], 201);


                    } else {
                        return response()->json(['result' => ['status' => 'false', 'massage' => 'error in db connection']], 401);
                    }
                }
            }

        } else {
            return response()->json(['result' => ['status' => 'false', 'massage' => 'email not found']], 401);
        }




    }


}
