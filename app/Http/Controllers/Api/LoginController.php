<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use JWTAuth;


class LoginController extends Controller
{
    public function authenticate(Request $request)
    {

        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$auth_token = JWTAuth::attempt($credentials)) {
                return response()->json(['errors' => [["id" => "invalid_email_password", "message" => "invalid email or password"]]], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['errors' => 'could_not_create_token'], 500);
        }

        $user = \Auth::User();

        // all good so return the token
        return response()
            ->json(['result' => ['user_id'=>$user->id,'auth_token' => $auth_token,]], 200);
    }


    public function test()
    {
        dd('working');
    }


}
