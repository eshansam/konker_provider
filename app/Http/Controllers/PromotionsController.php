<?php

namespace App\Http\Controllers;

use App\Offers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PromotionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function promotions()
    {
        $offers = Offers::select('*')->get();

        return view('pages.promotions')
            ->with(['offers' => $offers]);
    }

    public function createPromotion()
    {
        return view('pages.create_promotion');
    }

    public function savePromotion(Requests\CreatePromotionRequest $request)
    {

        $destinationPath = 'uploads';
        $extension = Input::file('image')->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;

        Input::file('image')->move($destinationPath, $fileName);
        $url = '/uploads/' . $fileName;

        $domain = 'http://konker.arimaclanka.com';

        $offer = new Offers;
        $offer->name = $request->name;
        $offer->offer = $request->offer;
        $offer->description = $request->description;
        $offer->img_url = $domain.$url;
        $result = $offer->save();

        if ($result) {
            return redirect('/promotions');
        }
    }


    public function deletePromotion(Request $request)
    {


        $delete3 = Offers::where('id', $request->offer_id)->delete();
        if ($delete3) {
            return "SUCCESS";
        }

        return "FAIL";
    }


}
