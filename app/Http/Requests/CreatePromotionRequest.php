<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'offer'=>'required',
            'description'=>'required',
            'image'=>'required',

        ];
    }

    public function messages()
    {

        return [
            'name.required' => 'name is required.',
            'offer.required' => 'offer is required.',
            'description.required'=>'Discription name is required.',
            'image.required'=>'Image is required.',

        ];
    }
}
