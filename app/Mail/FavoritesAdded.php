<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FavoritesAdded extends Mailable
{
    use Queueable, SerializesModels;

    public $offer;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($offer, $email)
    {
        $this->offer = $offer;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $offer = $this->offer;
        $email = $this->email;

        return $this->subject('Konker Game by Hemas')->from('konker.game.hemas@gmail.com','Konker Game')->
        view('pages.fav_mail',compact('offer','email'));
    }
}
